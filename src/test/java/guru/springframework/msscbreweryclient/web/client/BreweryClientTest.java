package guru.springframework.msscbreweryclient.web.client;

import guru.springframework.msscbreweryclient.web.model.BeerDto;
import guru.springframework.msscbreweryclient.web.model.CustomerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URI;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BreweryClientTest {

    @Autowired
    BreweryClient client;

    //these tests have to run when the server(CustomerApplication ) running
    @Test
    void getBeerById() {
        BeerDto dto = client.getBeerById((UUID.randomUUID()));
        assertNotNull(dto);
        //test to call http://localhost:8089/api/v1/customer/00d711e3-5dbd-4f04-a687-9c87fdd7edb6
        //client(svc) call server(svc) : CustomerApplication
        //get data from server.

    }

    //integration test: data can got from client
    //these tests have to run when the server(CustomerApplication ) running
    @Test
    void saveNewBeer() {
        BeerDto beerDto = BeerDto.builder().beerName("new Beer").build();
        URI uri = client.saveNewBeer(beerDto);
        assertNotNull(uri);
        System.out.println(uri.toString());
        //  /api/v1/beer/d8a9fa6b-a14b-49d7-9450-33abdb2b0cb6
    }
    //these tests have to run when the server(CustomerApplication ) running
    @Test
    void updateBeer() {
        BeerDto beerDto = BeerDto.builder().beerName("new Beer").build();
        client.updateBeer(UUID.randomUUID(),beerDto);
    }

    @Test
    void deleteBeer(){
        client.deleteBeer(UUID.randomUUID());
    }

    //assignment
    //client: CustomerApplication 1st app we created
    @Test
    void getCustomerById(){
        CustomerDto customerDto = client.getCustomerById(UUID.randomUUID());
        assertNotNull(customerDto);
    }

    @Test
    void saveNewCustomer() {
        CustomerDto customerDto = CustomerDto.builder().name("new Customer").build();
        URI uri = client.saveNewCustomer(customerDto);
        assertNotNull(uri);
        System.out.println(uri.toString());
        //  /api/v1/beer/d8a9fa6b-a14b-49d7-9450-33abdb2b0cb6
    }

    @Test
    void updateCustomer() {
        CustomerDto customerDto = CustomerDto.builder().name("new Customer").build();
        client.updateCustomer(UUID.randomUUID(),customerDto);
    }

    @Test
    void deleteBeerCustomer(){
        client.deleteCustomer(UUID.randomUUID());
    }


}